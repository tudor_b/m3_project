# Import flask and template operators
from flask import Flask, render_template, url_for, session as u_session
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect, CSRFError
import os

# Define the WSGI application object
EMPTY_EMAIL = 'E-mail field is empty'
EMPTY_PASSWORD = 'Password field is empty'
USER_EXISTS = 'An account for this email already exists'
USER_CREATED = 'Account successfully created'
AUTH_SUCCES = 'You successfully log in'
AUTH_FAIL = 'Log in fail,E-mail or pass incorrect'
PASSWORD_MISMATCH = "Retype pass doesn't match"

dashboard_b = '''<a href="javascript:{}" onclick="document.getElementById('logout').submit(); return false;">Logout</a>'''
logout_b = '''<a href="javascript:{}" onclick="document.getElementById('dashboard').submit(); return false;">Dashboard</a>'''
login_b = '''<a href="/signin/" class='login'>Login</a>'''

app = Flask(__name__)

# Configurations
app.config.from_object('config')
# app.config['SQLALCHEMY_DATABASE_URI'] = os.environ["DATABASE_URL"]
csrf = CSRFProtect(app)

db = SQLAlchemy(app)

# Routes from every module

from app.mod_url.controllers import mod_url as url_module

from app.mod_auth.controllers import mod_auth as auth_module

app.register_blueprint(url_module)

app.register_blueprint(auth_module)


@app.route('/about')
def about():
    style = url_for('static', filename='style.css')
    if 'email' in u_session:
        return render_template('about.html', style=style,dashboard=dashboard_b, logout=logout_b), 200
    return render_template('about.html', style=style, login=login_b), 200


@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


@app.errorhandler(CSRFError)
def handle_csrf_error(e):
    style = url_for('static', filename='style.css')
    return render_template('index.html', link=e.description, style=style), 400

db.create_all()


