from app import db


class Url(db.Model):
    __tablename__ = 'urls'

    link_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    long_link = db.Column(db.String)
    user_email = db.Column(db.String)

    def __init__(self, long_link, user_email=None):
        self.long_link = long_link
        self.user_email = user_email



