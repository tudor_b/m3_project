from flask import render_template, redirect, Blueprint, request, abort, url_for, session as u_session
from app.mod_url.models import Url
from app import dashboard_b, logout_b, login_b
from app import db, csrf
from pdb import set_trace
import sys
import base64

mod_url = Blueprint('url', __name__,template_folder='../templates')
links = dict()
ids = dict()
link_id = 0


r_link = "localhost:8080/" # "https://stagiu-url.herokuapp.com/" #"localhost:8080/" #


@mod_url.route('/', defaults={'code': None}, methods=['GET', 'POST'])
@mod_url.route('/<code>', methods=['GET', 'POST'])
def home(code):
    global link_id

    if code is None:
        if request.method == "GET":
            style = url_for('static', filename='style.css')
            if 'email' in u_session:
                return render_template('index.html', style=style, link=u_session.pop('link', ''), dashboard=dashboard_b, logout=logout_b)

            return render_template('index.html', style=style, link=u_session.pop('link', ''), login=login_b)

        elif request.method == "POST":
            link = request.form['link']

            i = db.session.query(Url).filter_by(long_link=link).first()

            if i is None:
                if 'email' in u_session.keys():
                    row_to_insert = Url(link, u_session['email'])
                else:
                    row_to_insert = Url(link)

                db.session.add(row_to_insert)
                db.session.commit()

                i = db.session.query(Url).filter_by(long_link=link).first()

                c_id = base64.b64encode(str(i.link_id).encode())
            else:
                c_id = base64.b64encode(str(i.link_id).encode())
            u_session['link'] = r_link + c_id.decode().rstrip('=')
            return redirect('/')
    else:
        try:
            code = str(code)
            padding = len(code) % 4
            code += '=' * padding
            row_id = int(base64.b64decode(code.encode()))
            rez_row = db.session.query(Url).get(row_id)
            link_to_redirect = rez_row.long_link
        except Exception as e:
            link_to_redirect = 'n'
            print('exceptie', e, file=sys.stderr)
            abort(404)
        # set_trace()
        if link_to_redirect.startswith('http://') or link_to_redirect.startswith('https://'):
            return redirect(link_to_redirect)
        else:
            return redirect('http://'+link_to_redirect)


@mod_url.route('/nologin/')
def nologin():
    style = url_for('static', filename='style.css')
    return render_template('nologin.html', style=style)

if __name__ == "__main___":

    print('main')