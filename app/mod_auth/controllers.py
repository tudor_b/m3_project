from flask import render_template, redirect, Blueprint, request, abort, url_for, session as u_session
import pdb
from app import USER_EXISTS,USER_CREATED, AUTH_SUCCES, AUTH_FAIL, EMPTY_EMAIL, EMPTY_PASSWORD,PASSWORD_MISMATCH
# from sqlalchemy.orm import Session
from app.mod_auth.models import User
from app.mod_url.models import Url
from app import dashboard_b, logout_b, login_b

from app import db, csrf
import bcrypt

mod_auth = Blueprint('auth', __name__, template_folder='../templates')


@mod_auth.route("/signin/", methods=['GET', 'POST'])
@csrf.exempt
def signin():

    style = url_for('static', filename='style.css')
    if request.method == "GET":

        return render_template('signin.html', style=style, link=u_session.pop('link', ''), login=login_b)

    if request.method == "POST":
        u_session.pop('email', None)
        u_email = str(request.form['email'])
        u_pass = str(request.form['pass'])

        if len(u_email) == 0:
            u_session['link'] = EMPTY_EMAIL
            return redirect('/signin/')

        if len(u_pass) == 0:
            u_session['link'] = EMPTY_PASSWORD
            return redirect('/signin/')

        user = db.session.query(User).filter_by(email=u_email).first()

        if user is None:
            u_session['link'] = AUTH_FAIL
            return redirect('/signin/')
        else:
            if bcrypt.hashpw(u_pass.encode(), user.password) == user.password:
                u_session['email'] = u_email
                return redirect('/dashboard/')
            else:
                u_session['link'] = AUTH_FAIL
                return redirect('/signin/')


@mod_auth.route("/signup/", methods=['GET', "POST"])
@csrf.exempt
def signup():

    if request.method == "GET":
        style = url_for('static', filename='style.css')
        return render_template('signup.html', style=style, link=u_session.pop('link', ''), login=login_b)

    if request.method == "POST":
        u_email = str(request.form['email'])
        u_pass = str(request.form['pass'])
        re_u_pass = str(request.form['repass'])

        if len(u_email) == 0:
            u_session['link'] = EMPTY_EMAIL
            return redirect('/signup/')

        if len(u_pass) == 0:
            u_session['link'] = EMPTY_PASSWORD
            return redirect('/signup/')

        if u_pass != re_u_pass:
            u_session['link'] = PASSWORD_MISMATCH
            return redirect('/signup/')

        user = db.session.query(User).filter_by(email=u_email).first()
        if user is None:

            hashed = bcrypt.hashpw(u_pass.encode(), bcrypt.gensalt())

            db.session.add(User(hashed, u_email))
            db.session.commit()
            u_session['link'] = USER_CREATED
            return redirect('/signup/')
        else:
            u_session['link'] = USER_EXISTS
            return redirect('/signup/')

    return render_template('404.html')


@mod_auth.route('/logout/', methods=['POST'])
def logout():
    if 'email' in u_session.keys():
        u_session.pop('email', None)
    return redirect('/')


@mod_auth.route('/dashboard/', methods=['POST', 'GET'])
def dashboard():

    if request.method == 'GET':
        if 'email' in u_session.keys():

            user_urls = db.session.query(Url).filter_by(user_email=u_session['email']).all()

            style = url_for('static', filename='style.css')

            return render_template('dashboard.html', style=style, urls=user_urls, dashboard=dashboard_b, logout=logout_b)
        else:
            return redirect('/')

    else:
        return redirect('/dashboard/')
