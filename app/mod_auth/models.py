from app import db


class User(db.Model):
    __tablename__ = 'users'

    password = db.Column(db.LargeBinary)
    email = db.Column(db.String, primary_key=True)

    def __init__(self, password, email):
        self.password = password
        self.email = email

