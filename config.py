# Statement for enabling the development environment
DEBUG = True

# Define the application directory
import os
from sqlalchemy import create_engine
from flask_sqlalchemy import declarative_base
from sqlalchemy.orm import  sessionmaker


BASE_DIR = os.path.abspath(os.path.dirname(__file__))  

# Define the database - we are working with
# SQLite for this example
import psycopg2
from urllib import parse

SQLALCHEMY_DATABASE_URI = 'sqlite:///local.db'
# SQLALCHEMY_DATABASE_URI = os.environ["DATABASE_URL"]


#db = create_engine(DB_URL)

# base = declarative_base()

# Session = sessionmaker(bind=db)



# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED = True

# Use a secure, unique and absolutely secret key for
# signing the data. 
CSRF_SESSION_KEY = "secret"

# Secret key for signing cookies
SECRET_KEY = "secret"